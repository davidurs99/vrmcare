@extends('layouts.app')

@section('content')

<div class="breadcrumb-container">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{url('/')}}">Home</a></li>
      <li class="breadcrumb-item active">Vision</li>
    </ol>
  </div>
</div>

<section class="pv-40 fixed-bg dark-translucent-bg hovered background-img-vision">
  <div class="container">
    <div class="row justify-content-lg-center">
      <div class="col-lg-8">
        <div class="testimonial text-center">
          <h3>Our Pillars - Vision, Mission, Values!</h3>
          <div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
          <p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">VRM CARE Ltd's Vision, Mission and Values are more than just statements of intent; they are the pillars of the business that unite us and enable us to focus on one common objective. They are also the fundamental principles of our approach and the promise we make to our people, our clients and our candidates.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="main-container">
  <div class="container">
    <div class="row">

      <!-- main start -->
      <!-- ================ -->
      <div class="main col-12">
        <div class="row">
          <div class="col-lg-6">
            <div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
              <span class="icon without-bg"><i class="fa fa-diamond"></i></span>
              <div class="body">
                <h4 class="title">VISION</h4>
                <p>VRM CARE Limited is to be the impetus for successful  medical staff resourcing in UK markets: bringing together medical staff and healthcare providers with maximum efficiency and safety for the provision of care to patients. We are dedicated to serving our customers and the community with the highest levels of service, knowledge, professionalism, honesty and integrity.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="150">
              <span class="icon without-bg"><i class="fa fa-connectdevelop"></i></span>
              <div class="body">
                <h4 class="title">VALUES</h4>
                <p>VRM CARE Ltd's promise is to offer an exceptional service with the highest standards of safety, ethics and governance in meeting the demands of clients, candidates and staff alike, with open and responsive communication, within an ethos of equal opportunity and environmental responsibility.</p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>

<section class="object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">
        <div class="full-width-section">
          <div class="full-image-container hovered">
            <img class="to-right-block" src="{{asset('images/collaborate.jpg')}}" alt="">
            <div class="full-image-overlay text-right">
              <h3 class="mt-4">We <strong>AIM TO</strong></h3>
              <ul class="list-icons">
                <li>Understand demands, challenges and targets<i class="fa fa-check-square-o"></i></li>
                <li>Offer flexible service <i class="fa fa-check-square-o"></i></li>
                <li>Continue in service provision. <i class="fa fa-check-square-o"></i></li>
                <li>Recruit and develop the best <i class="fa fa-check-square-o"></i></li>
                <li>Promote and develop new solutions <i class="fa fa-check-square-o"></i></li>
              </ul>
            </div>
          </div>
          <div class="full-text-container light-gray-bg border-bottom-clear">
            <h2><strong>MISSION</strong></h2>
            <div class="separator-2 hidden-lg-down"></div>
            <p>VRM CARE Ltd is to be the medical staffing agency of choice for healthcare providers and for temporary medical staff, based on the recognition of our delivery of a premier service, with safety paramount.</p>
            <p> VRM CARE Ltd's mission is to:</p>
            <ul class="normal-text">
                <li>Understand the medical staffing demands and challenges of our clients; and the career needs and targets of our medical staff candidates</li>
                <li>Offer a responsive flexible and helpful service 365 days of the year, to all selected geographies and sectors to fulfil these needs </li>
                <li>Continue to establish and be at the lead in service provision. </li>
                <li>Recruit and develop the most proficient teams and people at VRM CARE Ltd as consultants, researchers, ICT, compliance and support staff </li>
                <li>Promote the cause of flexible staffing and develop new solutions, to the benefit of healthcare providers, medical staff and patients </li>
              </ul>
            <div class="separator-3 hidden-lg-down"></div>
            <a href="{{url('/contact')}}" class="btn btn-default btn-animated">Contact Us <i class="pl-1 fa fa-envelope-o"></i></a>
          </div>
        </div>
      </section>
<!-- main-container end -->

@endsection