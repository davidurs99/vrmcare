@extends('layouts.app')

@section('content')
<section class="main-container padding-bottom-clear">
  <div class="container">
    <div class="row">
      <div class="main col-12">
        <h3 class="title">Who <strong>We Are</strong></h3>
        <div class="separator-2"></div>
        <div class="row">
          <div class="col-lg-6">
            <p>Established in 2014, VRM CARE Ltd is a professional Healthcare recruitment agency that is poised for providing well trained and dedicated healthcare staffing, excellent service delivery for a long lasting client satisfaction. </p>
            <p>Situated in Leighton Buzzard, we provide our superior quality staffing services to our valued clients (including private nursing/residential care homes, Bupa managed homes and the NHS Trusts)  across The United Kingdom.</p>
            <div class="separator-2"></div>
            <h3 class="title">Our <strong>Services</strong></h3>
            <p>We recruit for, and are able to provide a variety of health care workers including: </p>
            <ul class="list-icons">
              <li><i class="fa fa-check-square-o"></i> Doctors, Physiotherapists, NHS Trust</li>
              <li><i class="fa fa-check-square-o"></i> Registered Nurses, Social workers, Health Visitors</li>
              <li><i class="fa fa-check-square-o"></i> Care Assistants and Support Workers to the Local Authorities</li>
              <li><i class="fa fa-check-square-o"></i> Nursing/Residential Care Homes and Private hospitals</li>
            </ul>
          </div>
          <div class="col-lg-6">
            <div class="slick-carousel content-slider-with-controls">
              <div class="overlay-container overlay-visible">
                <img src="{{asset('images/patient.jpg')}}" alt="">
                <div class="overlay-bottom hidden-sm-down">
                  <div class="text">
                    <h3 class="title">We Can Do It</h3>
                  </div>
                </div>
              </div>
              <div class="overlay-container overlay-visible">
                <img src="{{asset('images/doctor-laptop.jpg')}}" alt="">
                <div class="overlay-bottom hidden-sm-down">
                  <div class="text">
                    <h3 class="title">You Can Trust Us</h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="margin-top: 2rem;">
                <div class="col-12">
                    <div class="separator-2"> </div>
                    <h4>If you are up to the challenge please contact on 08001937372</h4>
                </div>
            </div>
          </div>
        </div>
              <h3 class="title"><strong>Recruitment</strong></h3>
              <p>VRM CARE Ltd is always keen to recruit the best professionals with the right mix of professional experience and personality, which will add value while sustaining our client satisfaction ethos. </p>
              <p class="title"> Our recruitment practices and processes are fair and reasonable. All candidates are recruited according to their ability to do the job, whilst meeting the Agency laid down criteria, standards and appropriate legislations.</p>
            </div>
        </div>
      </div>
      <!-- main end -->
    </div>
  </div>
</section>
<!-- main-container end -->


@endsection