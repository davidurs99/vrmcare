@extends('layouts.app')

@section('content')

<div id="collapseMap" class="banner collapse">
  <!-- google map start -->
  <!-- ================ -->
  <div id="map-canvas"></div>
  <!-- google maps end -->
</div>
<!-- banner end -->

<!-- breadcrumb start -->
<!-- ================ -->
<div class="breadcrumb-container">
  <div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{url('/')}}">Home</a></li>
      <li class="breadcrumb-item active">Contact Page</li>
    </ol>
  </div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container">

  <div class="container">
    <div class="row">

      <!-- main start -->
      <!-- ================ -->
      <div class="main col-lg-8">
        <!-- page-title start -->
        <!-- ================ -->
        <h1 class="page-title">Contact Us</h1>
        <div class="separator-2"></div>
        <!-- page-title end -->
        <p>It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!</p>
        <div class="contact-form">
          <form id="contact-form" class="margin-clear">
            <div class="form-row">
              <div class="form-group has-feedback col-md-6">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                <i class="fa fa-user form-control-feedback"></i>
              </div>
              <div class="form-group has-feedback col-md-6">
                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                <i class="fa fa-envelope form-control-feedback"></i>
              </div>
            </div>
            <div class="form-group has-feedback">
              <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
              <i class="fa fa-navicon form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback">
              <textarea class="form-control" rows="6" id="message" name="message" placeholder="Message"></textarea>
              <i class="fa fa-pencil form-control-feedback"></i>
            </div>
            <input type="submit" value="Submit" class="submit-button btn btn-lg btn-default">
          </form>
        </div>
        <div id="success-alert" class="alert alert-success alert-dismissible hidden" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> We received your email. We will get back to you as soon as possible.
        </div>
      </div>
      <!-- main end -->

      <!-- sidebar start -->
      <!-- ================ -->
      <aside class="col-lg-4 col-xl-3 ml-xl-auto">
        <div class="sidebar">
          <div class="block clearfix">
            <h3 class="title">Find Us</h3>
            <div class="separator-2"></div>
            <ul class="list">
              <li><i class="fa fa-home pr-10"></i>3 Sparrow Path, Leighton Buzzard<br><span class="pl-20">Bedfordshire, LU7 4DE</span></li>
              <li><i class="fa fa-phone pr-10"></i><abbr title="Phone">P:</abbr> 0800 193 7372</li>
              <li><i class="fa fa-mobile pr-10 pl-1"></i><abbr title="Phone">M:</abbr> 07444734030</li>
              <li><i class="fa fa-envelope pr-10"></i><a>admin@vrmcare.co.uk</a></li>
            </ul>
          </div>
        </div>
        <div class="sidebar">
          <div class="block clearfix">
            <h2 class="title">Follow Us</h2>
            <div class="separator-2"></div>
            <ul class="social-links circle small margin-clear clearfix animated-effect-1">
              <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>
          </div>
        </div>
      </aside>
      <!-- sidebar end -->

    </div>
  </div>
</section>

@endsection