@extends('layouts.app')

@section('content')


<div class="banner pv-40 dark-translucent-bg hovered" style="background-image: url('images/banner-2.jpg');">

  <div class="video-background-poster" ></div>

  <!-- breadcrumb start -->
  <!-- ================ -->
  <div class="breadcrumb-container">
    <div class="container">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">Home</a></li>
        <li class="breadcrumb-item active">Work with us</li>
      </ol>
    </div>
  </div>
  <!-- breadcrumb end -->
  <div class="container">
    <div class="row justify-content-lg-center">
      <div class="col-lg-8 text-center pv-20">
        <h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">From a job to a career with VRM Care</h2>
        <div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
        <p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">
        	VRM CARE Ltd  is one of the UK’s fast growing provider of staffing services in the health and social care sector, with a staff that has a proven track record of compassionate care.
        </p>
      </div>
    </div>
  </div>

</div>
<!-- banner end -->

<!-- main-container start -->
<!-- ================ -->
<section class="main-container border-clear light-gray-bg padding-bottom-clear">

  <div class="container">
    <div class="row">

      <!-- main start -->
      <!-- ================ -->
      <div class="main col-12">

        <!-- page-title start -->
        <!-- ================ -->
        <h1 class="page-title text-center">Work with us</h1>
        <!-- page-title end -->

        <div class="image-box space-top style-4">
          <div class="row grid-space-0">
            <div class="col-lg-6">
              <div class="overlay-container">
                <img src="{{asset('images/nurse-patient.jpg')}}" alt="">
                <div class="overlay-to-top">
                  <p class="small margin-clear"><em>Work with <br> specialists.</em></p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="body">
                <div class="pv-30 hidden-lg-down"></div>
                <h3>Highly qualified stuff</h3>
                <div class="separator-2"></div>
                <p class="margin-clear">VRM CARE recruits highly  qualified staff that are keen to promote and restore patients' health by completing the nursing process; collaborating with physicians and multidisciplinary team members; providing physical and psychological support to patients, friends, and families; supervising assigned team members. We believe that the staff we employ are amongst the very finest and well-qualified within their chosen professions.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="image-box style-4">
          <div class="row grid-space-0">
            <div class="col-lg-6 order-lg-2">
              <div class="overlay-container">
                <img src="{{asset('images/doctor-thinking.jpg')}}" alt="">
                <div class="overlay-to-top">
                  <p class="small margin-clear"><em>Want to find more? <br> Call us.</em></p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="body text-right">
                <div class="pv-30 hidden-lg-down"></div>
                <h3>Feel you are the right person?</h3>
                <div class="separator-3"></div>
                <p class="margin-clear">VRM CARE is always on the lookout for passionate and committed individuals who want a rewarding career in health and social care. If you think you have the right work experience and qualifications, please post us your CV or call our recruitment team. </p>
                <br>
                <a href="/register" class="btn btn-lg btn-gray-transparent btn-sm btn-animated margin-clear">Start now<i class="fa fa-arrow-right pl-10"></i></a>
              </div>
            </div>
          </div>
        </div>


    </div>

  </div>
</section>
@include('specialties-banner')
<!-- main-container end -->

@endsection