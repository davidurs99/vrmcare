@extends('layouts.app')

@section('content')
<div class="breadcrumb-container">
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="index.html">Home</a></li>
			<li class="breadcrumb-item active">Page Login</li>
		</ol>
	</div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<div class="main-container dark-translucent-bg" style="background-image:url('images/background-img-6.jpg');">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<!-- main start -->
				<!-- ================ -->
				<div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
					<div class="form-block p-30 light-gray-bg border-clear">
						<h2 class="title">Login</h2>
						<form id="login-form" class="form-horizontal">
							<div class="form-group has-feedback row">
								<label for="email" class="col-md-3 text-md-right control-label col-form-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" id="email" placeholder="Email" required>
									<i class="fa fa-user form-control-feedback pr-4"></i>
								</div>
							</div>
							<div class="form-group has-feedback row">
								<label for="inputPassword" class="col-md-3 text-md-right control-label col-form-label">Password</label>
								<div class="col-md-8">
									<input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password" required>
									<i class="fa fa-lock form-control-feedback pr-4"></i>
								</div>
							</div>

							<div class="form-group row">
							  <div class="ml-md-auto col-md-9">
							    <button type="submit" class="btn btn-group btn-default btn-animated">Log In <i class="fa fa-user"></i></button>
							  </div>
							</div>
						</form>
					</div>
				</div>
				<!-- main end -->
			</div>
		</div>
	</div>
</div>
@endsection