<div>
	You have received a new message
	@if ($name)
		Name: {{$name}}<br>
		Email: {{$email}}<br>
		Sector: {{$sector}}<br>
		Number: {{$number}}<br>
		Company: {{$companyName}}<br>
		Company Size: {{$companySize}}<br>
		Profit: {{$profit}}<br>
		@if ($other)
			Other info: {{$other}}<br>
		@endif
	@endif

	@if ($phone)
		Phone: {{$phone}}<br>
	@endif
</div>