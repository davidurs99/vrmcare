<div>
	You have received a new contact message from <b><i>{{$name}}</i></b> ({{$email}})
	@if ($subject)
			With the subject: {{$subject}}
	@endif

	<br><br>
	{{$msg}}
</div>