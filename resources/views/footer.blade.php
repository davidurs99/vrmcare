<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
<!-- ================ -->
<footer id="footer" class="clearfix dark">

	<!-- .footer start -->
	<!-- ================ -->
	<div class="footer">
		<div class="container">
			<div class="footer-inner">
				<div class="row justify-content-lg-center">
					<div class="col-lg-6">
						<div class="footer-content text-center padding-ver-clear">
							<div class="logo-footer logo-font logo">
								<img src="{{asset('images/logo_new.png')}}">
							</div>
							<p>Healthcare recruitment agency.</p>
							<ul class="list-inline mb-20">
								<li class="list-inline-item"><i class="text-default fa fa-map-marker pr-1"></i>3 Sparrow Path, Leighton Buzzard, Bedfordshire, LU7 4DE</li>
								<br>
								<li class="list-inline-item"><i class="text-default fa fa-phone pl-10 pr-1"></i>0800 193 7372</li>
								<li class="list-inline-item"><i class="text-default fa fa-mobile pl-10 pr-1"></i>07444734030</li>
								<li class="list-inline-item"><a href="{{url('/')}}/contact" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-1"></i>admin@vrmcare.co.uk</a></li>
							</ul>
							<div class="separator"></div>
							<p class="text-center margin-clear">Copyright &copy; 2018 VRM Care. All Rights Reserved</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- .footer end -->

</footer>
      <!-- footer end -->