@extends('layouts.app')

@section('content')
<?php $headers = ['Band', 'Day', 'Night', 'Sat', 'Sun', 'Public Hols']; ?>
<?php $headersSouth = ['', 'WEEKDAY', 'NIGHTS/SATURDAY', 'SUNDAY', 'BANK HOL']; ?>

<section class="main-container">

	<div class="container">
		<div class="row">

			<!-- main start -->
			<!-- ================ -->
			<div class="main col-lg-12">

				<!-- page-title start -->
				<!-- ================ -->
				<h1 class="page-title">Payrates</h1>
				<br>

				<ul class="nav nav-tabs style-1" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" href="#htab1" role="tab" data-toggle="tab"></i>Payrates North</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#htab2" role="tab" data-toggle="tab"></i>Payrates South</a>
					</li>
				</ul>

				<div class="tab-pane show active" id="htab1" role="tabpanel">
					<h3 class='table-section-title'>Pay Rates - North of England, Scotland and Wales</h3>
					<p>Nurse and healthcare assistant jobs are available for all medical field professionals who want to work with a leading team of medical experts. Our advance compliance systems and network of branches deliver first class service to healthcare professionals who are seeking work in Lancashire, Cheshire, South Yorkshire. With the latest ICT and training systems we are able to deliver first class service to medical staff in the regions of Scotland and Wales</p>

					<div class="separator-2"></div>
					<!-- page-title end -->

					<!-- Tables start -->
					<!-- ============================================================================== -->
					<div id="nursing-homes" class="table-section">
						<p class="mt-4">Nursing Homes / Hospice Rates of Pay</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5 General</td>
									<td>25.40</td>
									<td>26.88</td>
									<td>31.50</td>
									<td>35.31</td>
									<td>40.82</td>
								</tr>
								<tr>
									<td>6 General</td>
									<td>27.08</td>
									<td>28.36</td>
									<td>39.92</td>
									<td>36.60</td>
									<td>41.46</td>
								</tr>
								<tr>
									<td>7 General</td>
									<td>27.63</td>
									<td>29.09</td>
									<td>33.05</td>
									<td>38.04</td>
									<td>42.29</td>
								</tr>
								<tr>
									<td>5 Mental</td>
									<td>26.89</td>
									<td>29.77</td>
									<td>32.85</td>
									<td>36.51</td>
									<td>41.30</td>
								</tr>
								<tr>
									<td>6 Mental</td>
									<td>29.12</td>
									<td>30.43</td>
									<td>35.01</td>
									<td>38.40</td>
									<td>43.44</td>
								</tr>
								<tr>
									<td>5 General</td>
									<td>25.40</td>
									<td>26.88</td>
									<td>31.50</td>
									<td>35.31</td>
									<td>44.16</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Occupational Health, Community, Practice, District, School and Industry Nurse Rates of Pay</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5 General/Mental</td>
									<td>34.50</td>
									<td>36.09</td>
									<td>38.45</td>
									<td>43.16</td>
									<td>50.20</td>
								</tr>
								<tr>
									<td>6 General/Mental</td>
									<td>35.27</td>
									<td>36.80</td>
									<td>39.83</td>
									<td>44.38</td>
									<td>51.19</td>
								</tr>
								<tr>
									<td>7 General/Mental</td>
									<td>36.78</td>
									<td>38.31</td>
									<td>41.35</td>
									<td>45.89</td>
									<td>52.70</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Mental Health Hospitals, Prisons and Psychiatric Reahbilitation Units Rates Of Pay</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5 General/Mental</td>
									<td>33.53</td>
									<td>34.97</td>
									<td>37.10</td>
									<td>41.30</td>
									<td>46.89</td>
								</tr>
								<tr>
									<td>6 General/Mental</td>
									<td>36.06</td>
									<td>37.53</td>
									<td>40.46</td>
									<td>44.88</td>
									<td>50.76</td>
								</tr>
								<tr>
									<td>7 General/Mental</td>
									<td>41.42</td>
									<td>42.97</td>
									<td>46.04</td>
									<td>50.46</td>
									<td>52.27</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">General Hospitals Rate of Pay</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5 General/Mental</td>
									<td>33.09</td>
									<td>34.31</td>
									<td>36.74</td>
									<td>42.88</td>
									<td>45.30</td>
								</tr>
								<tr>
									<td>6 General/Mental</td>
									<td>33.83</td>
									<td>35.11</td>
									<td>38.34</td>
									<td>44.48</td>
									<td>46.83</td>
								</tr>
								<tr>
									<td>7 General/Mental</td>
									<td>35.13</td>
									<td>36.71</td>
									<td>39.95</td>
									<td>46.10</td>
									<td>48.34</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Critical Care / Speciality Rates of Pay</p>
						<p class="mt-4"><b>RSCN, Cardiac, ITU, HDU, A&E, Paeds, PICU, NICU, SCBU, Recovery, Scrub, Health Visitor, Oncology, Dialysis</b></p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5</td>
									<td>43.55</td>
									<td>44.87</td>
									<td>47.21</td>
									<td>51.84</td>
									<td>58.43</td>
								</tr>
								<tr>
									<td>6</td>
									<td>44.45</td>
									<td>45.68</td>
									<td>48.75</td>
									<td>53.35</td>
									<td>59.54</td>
								</tr>
								<tr>
									<td>7</td>
									<td>47.26</td>
									<td>51.35</td>
									<td>54.56</td>
									<td>58.30</td>
									<td>60.96</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Midwifery, ODA & ODP Rates of Pay</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5</td>
									<td>43.97</td>
									<td>45.86</td>
									<td>48.11</td>
									<td>52.53</td>
									<td>59.36</td>
								</tr>
								<tr>
									<td>6</td>
									<td>45.99</td>
									<td>47.49</td>
									<td>50.49</td>
									<td>55.00</td>
									<td>61.01</td>
								</tr>
								<tr>
									<td>7</td>
									<td>50.53</td>
									<td>52.06</td>
									<td>55.16</td>
									<td>58.92</td>
									<td>61.38</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<h2 class="table-section-title">Domicialiry rates of pay</h2>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">General Care in the Community</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5</td>
									<td>33.09</td>
									<td>34.31</td>
									<td>36.74</td>
									<td>42.88</td>
									<td>45.30</td>
								</tr>
								<tr>
									<td>6</td>
									<td>33.83</td>
									<td>35.11</td>
									<td>38.34</td>
									<td>44.48</td>
									<td>46.83</td>
								</tr>
								<tr>
									<td>7</td>
									<td>35.13</td>
									<td>36.71</td>
									<td>39.95</td>
									<td>46.10</td>
									<td>48.34</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Complex Care in the Community</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headers as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>5</td>
									<td>43.55</td>
									<td>44.87</td>
									<td>47.21</td>
									<td>51.84</td>
									<td>58.43</td>
								</tr>
								<tr>
									<td>6</td>
									<td>44.45</td>
									<td>45.68</td>
									<td>48.75</td>
									<td>53.35</td>
									<td>59.54</td>
								</tr>
								<tr>
									<td>7</td>
									<td>47.26</td>
									<td>51.35</td>
									<td>54.56</td>
									<td>58.30</td>
									<td>60.96</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Sleepover Rates (Per Hour) </p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									<th>MON-SAT</th>
									<th>SUN</th>
									<th>PUBLIC HOLS</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>11.78</td>
									<td>14.13</td>
									<td>17.67</td>
								</tr>
							</tbody>
						</table>
						<p>Risk Assessment and live-in rates are set on a per assignment basis.</p>
					</div>

					<div class="separator-2"></div>

					<h2 class="table-section-title">GUIDE TO PAY RATES FOR CARE STAFF</h2>

					<div class="separator-2"></div>


					<div class="table-section">
						<p class="mt-4">Care Assistant/Auxiliary/Residential Social Worker Indictive Pay Rates</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									<th>ALL BANDS</th>
									<th>Category A</th>
									<th>Category B</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Monday to Friday Day</td>
									<td>12.13</td>
									<td>14.26</td>
								</tr>
								<tr>
									<td>Monday to Friday Night</td>
									<td>13.35</td>
									<td>15.71</td>
								</tr>
								<tr>
									<td>Saturday</td>
									<td>14.55</td>
									<td>16.80</td>
								</tr>
								<tr>
									<td>Sunday</td>
									<td>16.98</td>
									<td>19.53</td>
								</tr>
								<tr>
									<td>Public Holiday</td>
									<td>19.40</td>
									<td>22.39</td>
								</tr>
							</tbody>
						</table>
						<p>Category A - Nursing and Residential Homes,  Community, Rehabilitation and Domiciliary.</p>
						<p>Category B - NHS, Private Hospitals, Psychiatric and Secure Units, Learning Disability, Challenging Behaviour, Acute and Domicilliary (complex).</p>
						<p>Subject to discussion with the client and to agreement with you before any shift commences mileage may be paid at the rate of 0.50 per mile. The amount of miles to be paid will be agreed in advance. Unless this covers travel between assignments any amount paid will be subject to HMR&C rules.</p>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">Care Assistant/Auxiliary/Residential Social Worker Indictive Pay Rates</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									<th>ALL BANDS</th>
									<th>Category A</th>
									<th>Category B</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Monday to Friday Day</td>
									<td>14.92</td>
									<td>18.73</td>
								</tr>
								<tr>
									<td>Monday to Friday Night</td>
									<td>16.97</td>
									<td>21.32</td>
								</tr>
								<tr>
									<td>Saturday</td>
									<td>18.34</td>
									<td>23.00</td>
								</tr>
								<tr>
									<td>Sunday</td>
									<td>21.41</td>
									<td>27.00</td>
								</tr>
								<tr>
									<td>Public Holiday</td>
									<td>25.78</td>
									<td>31.91</td>
								</tr>
							</tbody>
						</table>
						<p>Category A - Nursing and Residential Homes,  Community, Rehabilitation and Domiciliary.</p>
						<p>Category B - NHS, Private Hospitals, Psychiatric and Secure Units, Learning Disability, Challenging Behaviour, Acute and Domicilliary (complex).</p>
						<p>Subject to discussion with the client and to agreement with you before any shift commences mileage may be paid at the rate of 0.50 per mile. The amount of miles to be paid will be agreed in advance. Unless this covers travel between assignments any amount paid will be subject to HMR&C rules.</p>
					</div>

					<div class="separator-2"></div>

					<div class="table-section">
						<p class="mt-4">SLEEPOVER INDICITIVE PAY RATES (PER HOUR)</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									<th></th>
									<th>MON-SAT</th>
									<th>SUN</th>
									<th>PUBLIC HOLS</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Care Assistant (Category A and Category B)</td>
									<td>8.20</td>
									<td>8.72</td>
									<td>9.92</td>
								</tr>
								<tr>
									<td>Senior Care (Category A and Category B)</td>
									<td>9.79</td>
									<td>11.19</td>
									<td>11.89</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<h2 class="table-section-title">IMPORTANT NOTES</h2>

					<p>Effective: 26 March 2018</p>
					<p>Day rates paid on hours worked from 08:00 to 20:00. </p>
					<p>Night rates paid on hours worked from 20:00 to 08:00. </p>
					<p>Saturday rates paid on hours worked from 08:00 Saturday to 08:00 Sunday. </p>
					<p>Sunday rates paid on hours worked from 08:00 Sunday to 08:00 Monday.</p>

					<p>Bank Holiday rates paid on hours worked from 12pm midnight on the start of the holiday to 08:00 on the following day - with the exception of Christmas Eve and New Year's Eve where Bank Holiday rates will be paid on both days from 16:00 hours to 08:00 on the day following the Bank Holiday period. All time bands may be subject to change in order to comply with the Agency Workers Regulations.</p>

					<p>These pay rates are a guide only. In situations where in order to comply with the Agency Workers Regulations holiday pay over and above the basic entitlement becomes due then this excess shall be regarded as being included in the quoted pay rate (and hence paid as rolled up holiday pay).</p>
				</div>














				<div class="tab-pane fade" id="htab2" role="tabpanel">
					<h3 class='table-section-title'>Southern England Rates of Pay</h3>
					<p>The rates quoted below are guideline pay rates per hour/per shift. Your actual pay rate may differ from those noted below depending on our contractual arrangements with our clients.</p>
					<p>Please ensure that you confirm your pay rate and grade of booking with your consultant at the time of accepting a shift. In situation where in order to comply with the Agency Workers Regulations holiday pay over and above the basic entitlement becomes due then this excess shall be regarded as being included in the quoted pay rate (and hence paid as rolled up holiday pay).</p>
					<br>
					<p>EFFECTIVE: 26 March 2018</p>

					<div class="separator-2"></div>
					<!-- page-title end -->

					<!-- Tables start -->
					<!-- ============================================================================== -->
					<div id="nursing-homes" class="table-section">
						<h3 class="table-section-title">Specialist Nurses</h3>
						<p class="mt-4">ITU, HDU, CCU, A & E, Theatre, Recovery, Occupational Health, Health Visitors, Midwives, Prisons, Psychiatric, Cardiothoracic, ODP, Paediatrics,PICU, SCBU, NICU</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headersSouth as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Specialist Nurse Band 5</td>
									<td>35.33</td>
									<td>38.35</td>
									<td>39.52</td>
									<td>41.29</td>
								</tr>
								<tr>
									<td>Specialist Nurse Band 6</td>
									<td>36.52</td>
									<td>39.52</td>
									<td>40.71</td>
									<td>42.47</td>
								</tr>
								<tr>
									<td>Specialist Nurse Band 7</td>
									<td>38.93</td>
									<td>40.71</td>
									<td>41.88</td>
									<td>43.65</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div id="nursing-homes" class="table-section">
						<h3 class="table-section-title">General Nursing</h3>
						<p class="mt-4">Hospitals, Community and Complex Care</p>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headersSouth as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Healthcare Assistant Band 2</td>
									<td>12.51</td>
									<td>15.15</td>
									<td>19.01</td>
									<td>20.19</td>
								</tr>
								<tr>
									<td>Healthcare Assistant Band 3</td>
									<td>14.05</td>
									<td>16.43</td>
									<td>19.91</td>
									<td>21.11</td>
								</tr>
								<tr>
									<td>Nurse Band 5</td>
									<td>26.20</td>
									<td>28.58</td>
									<td>31.56</td>
									<td>32.75</td>
								</tr>
								<tr>
									<td>Nurse Band 6</td>
									<td>28.58</td>
									<td>30.95</td>
									<td>33.94</td>
									<td>35.13</td>
								</tr>
								<tr>
									<td>Nurse Band 7</td>
									<td>29.77</td>
									<td>32.15</td>
									<td>35.13</td>
									<td>36.91</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div id="nursing-homes" class="table-section">
						<h3 class="table-section-title">CARE HOMES</h3>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									@foreach ($headersSouth as $header)
									<th>{{$header}}</th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Healthcare Assistant</td>
									<td>11.61</td>
									<td>14.07</td>
									<td>15.70</td>
									<td>16.64</td>
								</tr>
								<tr>
									<td>Senior Healthcare Assistant</td>
									<td>13.21</td>
									<td>15.34</td>
									<td>17.64</td>
									<td>18.76</td>
								</tr>
								<tr>
									<td>RGN/RMN D Grade/Band 5</td>
									<td>24.66</td>
									<td>26.79</td>
									<td>28.31</td>
									<td>30.08</td>
								</tr>
								<tr>
									<td>RGN/RMN E Grade/Band 6</td>
									<td>25.37</td>
									<td>27.73</td>
									<td>29.20</td>
									<td>31.27</td>
								</tr>
								<tr>
									<td>RGN/RMN F Grade/Band 7</td>
									<td>26.54</td>
									<td>28.91</td>
									<td>30.38</td>
									<td>31.85</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div id="nursing-homes" class="table-section">
						<h3 class="table-section-title">Sleep-in - Healthcare Assistant</h3>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									<th></th>
									<th>MONDAY-SATURDAY</th>
									<th>SUNDAY</th>
									<th>BANK HOLIDAY</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Sleep-in - Healthcare Assistant</td>
									<td>8.11</td>
									<td>8.74</td>
									<td>9.36</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<div id="nursing-homes" class="table-section">
						<h3 class="table-section-title">Sleep-in - Nurse</h3>
						<table class="table table-striped table-colored">
							<thead>
								<tr>
									<th></th>
									<th>MONDAY-SATURDAY</th>
									<th>SUNDAY</th>
									<th>BANK HOLIDAY</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Sleep-in - Nurses</td>
									<td>11.24</td>
									<td>13.73</td>
									<td>14.98</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="separator-2"></div>

					<h2 class="table-section-title">IMPORTANT NOTES</h2>

					<p>Unless hours require adjustment in order to ensure compliance with the Agency Workers Regulations the following shift and charge patterns apply.</p>
					<br>

					<p>Day rates paid on hours worked from 08.00 to 20.00.</p>
					<p>Night rates paid on hours worked from 20.00 to 08.00.</p>
					<p>Saturday rates paid on hours worked from 08.00 Saturday to 08.00 Sunday.</p>
					<p>Sunday rates paid on hours worked from 08.00 Sunday to 08.00 Monday.</p>
					<p>Bank Holiday rates paid on hours worked from 12pm midnight on the start of the holiday to 08.00 on the following day - with the exception of Christmas Eve and New Year’s Eve where Bank Holiday rates will be paid on both days from 16.00 hours to 08.00 on the day following the Bank Holiday period</p>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection