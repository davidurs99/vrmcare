@extends('layouts.app')

@section('content')
<section class="main-container">

	<div class="container">
		<div class="row">

			<!-- main start -->
			<!-- ================ -->
			<div class="main col-lg-12">

				<!-- page-title start -->
				<!-- ================ -->
				<h1 class="page-title">Policies</h1>
				<div class="separator-2"></div>
				<p>If you would want to view any of the policies please contact us.</p>

				<div class="separator-2"></div>
				<!-- page-title end -->

				<!-- Tables start -->
				<!-- ============================================================================== -->
				<div class="table-section">
					<table class="table table-striped table-colored">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<!-- <th>Action</th> -->
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($policies as $policy)
							<?php
								$policy = str_replace('policies/', '', $policy);
								$pos1 = strpos($policy, ' ');
								$pos2 = strpos($policy, ' ', $pos1 + strlen(' '));
								$name = substr($policy, $pos2, -4);
								$id = substr($policy, 0, $pos2);
							?>
							<tr>
								<td>{{$id}}</td>
								<td>{{$name}}</td>
								<!-- <td>
									<a target="_blank" href="{{url('/storage')}}/{{$policy}}">View</a>
								</td> -->
								<td>
									@if (Auth::check()) <a target="_blank" href="{{url('/download')}}/{{$policy}}">Download</a>
									@else
									<a href="#" disabled>Download</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	@endsection