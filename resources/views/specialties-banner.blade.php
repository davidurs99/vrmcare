<section class="section dark-bg clearfix">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="call-to-action text-center">
					<div class="row">
						<div class="col-md-8">
							<h1 class="title"><i class="fa fa-user-md pr-10"></i>Looking for</h1>
							<ul class="list-inline">
								<li class="list-inline-item"><i class="fa fa-circle text-default"></i> <a href="#" class="link-light">Specialist Nurses</a></li>
								<li class="list-inline-item"><i class="fa fa-circle text-default"></i> <a href="#" class="link-light">General Nurses</a></li>
								<li class="list-inline-item"><i class="fa fa-circle text-default"></i> <a href="#" class="link-light">Pediatric Nurses</a></li>
								<li class="list-inline-item"><i class="fa fa-circle text-default"></i> <a href="#" class="link-light">Mental Health Nurses</a></li>
								<li class="list-inline-item"><i class="fa fa-circle text-default"></i> <a href="#" class="link-light">Healtcare workes</a></li>
							</ul>
						</div>
						<div class="col-md-4">
							<br>
							<p><a href="{{url('/about-us')}}" class="btn btn-lg btn-default btn-animated">More Specialties<i class="fa fa-search pl-20"></i></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>