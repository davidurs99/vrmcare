<!DOCTYPE html>
<html dir="ltr" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="VRM Care, Nursing Agency">
    <meta name="author" content="author">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'VRM') }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/logo_meta.png">

    <!-- Web Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{ asset('fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <!-- Plugins -->
    <link href="{{ asset('plugins/magnific-popup/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/rs-plugin-5/css/settings.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/rs-plugin-5/css/layers.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/rs-plugin-5/css/navigation.css')}}" rel="stylesheet">
    <link href="{{ asset('css/animations.css')}}" rel="stylesheet">
    <link href="{{ asset('plugins/slick/slick.css')}}" rel="stylesheet">

    <link href="{{ asset('css/style.css')}}" rel="stylesheet" >
    <link href="{{ asset('css/typography-default.css')}}" rel="stylesheet" >
    <link href="{{ asset('css/skins/dark_cyan.css')}}" rel="stylesheet">

    <!-- Custom css -->
    <link href="{{ asset('css/custom.css')}}" rel="stylesheet">

  </head>
</head>
<body>
    <div class="scrollToTop circle"><i class="fa fa-angle-up"></i></div>

    <div class="page-wrapper">
        @include('header')
        @yield('content')
        @include('footer')
    <div>
    <!-- Jquery and Bootstap core js files -->
    <script src="{{ asset('plugins/jquery.min.js')}}"></script>
    <script src="{{ asset('plugins/jquery-validate.min.js')}}"></script>
    <script src="{{ asset('js/login.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}"></script>

    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>

    <script>
        @if (isset($navHighlightId))
            var navHighlightId = '{{$navHighlightId}}';
        @else
            var navHighlightId = 'home';
        @endif
        @if (isset($submenuHighlightId))
            var submenuHighlightId = '{{$submenuHighlightId}}';
        @else
            var submenuHighlightId = 'home';
        @endif

    </script>

    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- jQuery Revolution Slider  -->
    <script src="{{ asset('plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{ asset('plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- Isotope javascript -->
    <script src="{{ asset('plugins/isotope/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('plugins/isotope/isotope.pkgd.min.js')}}"></script>
    <!-- Magnific Popup javascript -->
    <script src="{{ asset('plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <!-- Appear javascript -->
    <script src="{{ asset('plugins/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('plugins/waypoints/sticky.min.js')}}"></script>
    <!-- Count To javascript -->
    <script src="{{ asset('plugins/countTo/jquery.countTo.js')}}"></script>
    <!-- Google Maps javascript -->
    <!-- <script src="{{ asset('https://maps.googleapis.com/maps/api/js?key=your_google_map_key')}}"></script> -->
    <!-- <script src="{{ asset('js/google.map.config.js')}}"></script> -->
    <!-- Slick carousel javascript -->
    <script src="{{ asset('plugins/slick/slick.min.js')}}"></script>
    <!-- Initialization of Plugins -->
    <script src="{{ asset('js/template.js')}}"></script>
    <!-- Custom Scripts -->
    <script src="{{ asset('js/custom.js')}}"></script>

    @if (isset($scripts))
        @foreach ($scripts as $script)
            <script src="{{ asset($script)}}"></script>
        @endforeach
    @endif
</body>
</html>
