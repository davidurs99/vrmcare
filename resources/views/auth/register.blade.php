@extends('layouts.app')

@section('content')
<div class="breadcrumb-container">
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><i class="fa fa-home pr-2"></i><a class="link-dark" href="{{url('/')}}index.html">Home</a></li>
			<li class="breadcrumb-item active">Page Register</li>
		</ol>
	</div>
</div>
<!-- breadcrumb end -->

<!-- main-container start -->
<!-- ================ -->
<div class="main-container dark-translucent-bg" style="background-image:url('images/recruitment.jpg');">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<!-- main start -->
				<!-- ================ -->
				<div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
					<div class="form-block p-30 light-gray-bg border-clear">
						<h2 class="title">Register</h2>
						<form id="register-form" class="form-horizontal">
							<div class="form-group has-feedback row">
								<label for="inputName" class="col-md-3 control-label text-md-right col-form-label">Full Name <span class="text-danger small">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="inputName" placeholder="Fisrt Name" name="name" required>
									<i class="fa fa-pencil form-control-feedback pr-4"></i>
								</div>
							</div>
							<div class="form-group has-feedback row">
								<label for="inputEmail" class="col-md-3 control-label text-md-right col-form-label">Email <span class="text-danger small">*</span></label>
								<div class="col-md-8">
									<input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email">
									<i class="fa fa-envelope form-control-feedback pr-4"></i>
								</div>
							</div>
							<div class="form-group has-feedback row">
								<label for="inputPhone" class="col-md-3 control-label text-md-right col-form-label">Phone <span class="text-danger small">*</span></label>
								<div class="col-md-8">
									<input name="phone" type="text" class="form-control" id="inputPhone" placeholder="Email">
									<i class="fa fa-phone form-control-feedback pr-4"></i>
								</div>
							</div>
							<div class="form-group has-feedback row" style="display: none">
								<label for="password" class="col-md-3 control-label text-md-right col-form-label">Password <span class="text-danger small">*</span></label>
								<div class="col-md-8">
									<input name="password" type="password" class="form-control" id="password" placeholder="Password" value="parola">
									<i class="fa fa-lock form-control-feedback pr-4"></i>
								</div>
							</div>
							<div class="form-group has-feedback row" style="display: none">
								<label for="password-confirmation" class="col-md-3 control-label text-md-right col-form-label">Confirm Password <span class="text-danger small">*</span></label>
								<div class="col-md-8">
									<input name="password_confirmation" type="password" class="form-control" id="password-confirmation" placeholder="Password" value="parola">
									<i class="fa fa-lock form-control-feedback pr-4"></i>
								</div>
							</div>

							{{-- <div class="form-group row">
								<div class="ml-md-auto col-md-9">
									<div class="checkbox form-check">
										<input class="form-check-input" type="checkbox" >
										<label class="form-check-label">
											Lorem ipsum <a href="#">dolor sit</a> and <a href="#">ametetot unamliage</a>
										</label>
									</div>
								</div>
							</div> --}}
							<div class="form-group row">
								<div class="ml-md-auto col-md-9">
									<button type="submit" class="btn btn-group btn-default btn-animated">Register <i class="fa fa-check"></i></button>
								</div>
							</div>
						</form>

						<div id="success-alert" class="alert alert-success alert-dismissible hidden" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Success!</strong> We received your registartion and will shortly contact you.
						</div>
					</div>
				</div>
				<!-- main end -->
			</div>
		</div>
	</div>
</div>
@endsection
