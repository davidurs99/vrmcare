@extends ('layouts.app')

@section('content')

<div class="banner clearfix">
	<div class="slideshow">
		<div class="slider-revolution-5-container">
			<div id="slider-banner-fullwidth-big-height" class="slider-banner-fullwidth-big-height rev_slider" data-version="5.0">
				<ul class="slides">
					<!-- slide 1 start -->
					<!-- ================ -->
					<li data-transition="random" data-slotamount="default" data-masterspeed="default" data-title="Wellcome">
						<!-- main image -->
						<img src="{{asset('images/care.jpeg')}}" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover"  class="rev-slidebg">

						<!-- Transparent Background -->
						<div class="tp-caption light-translucent-bg"
						data-x="center"
						data-y="center"
						data-start="0"
						data-transform_idle="o:1;"
						data-transform_in="o:0;s:600;e:Power2.easeInOut;"
						data-transform_out="o:0;s:600;"
						data-width="5000"
						data-height="5000">
						</div>

						<!-- LAYER NR. 1 -->
						<div class="tp-caption large_dark"
						data-x="left"
						data-y="220"
						data-start="500"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1150;e:Power4.easeInOut;"
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">Compassionate care
						</div>

						<!-- LAYER NR. 2 -->
						<div class="tp-caption medium_dark"
						data-x="left"
						data-y="290"
						data-start="750"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];sX:1;sY:1;s:850;e:Power4.easeInOut;"
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">We are keen to promote and restore patients' health.<br> We pride that among the staff we employ are amongst <br> the very finest and well-qualified within their chosen professions.
						</div>

						<!-- LAYER NR. 3 -->
						<div class="tp-caption small_dark"
						data-x="left"
						data-y="410"
						data-start="1000"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];sX:1;sY:1;o:0;s:600;e:Power4.easeInOut;"
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><a href="#" class="btn btn-dark btn-default btn-animated">Learn More <i class="fa fa-arrow-right"></i></a>
					</div>
				</li>
	<!-- slide 1 end -->

				<!-- slide 2 start -->
				<!-- ================ -->
				<li class="text-right" data-transition="random" data-slotamount="default" data-masterspeed="default" data-title="Vacancies">

					<!-- main image -->
					<img src="{{asset('/images/nurses-small.jpeg')}}" alt="slidebg2" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover" class="rev-slidebg">

					<!-- Transparent Background -->
					<div class="tp-caption dark-translucent-bg"
					data-x="center"
					data-y="center"
					data-start="0"
					data-transform_idle="o:1;"
					data-transform_in="o:0;s:600;e:Power2.easeInOut;"
					data-transform_out="o:0;s:600;"
					data-width="5000"
					data-height="5000">
					</div>

					<!-- LAYER NR. 1 -->
					<div class="tp-caption large_white"
						data-x="right"
						data-y="200"
						data-start="500"
						data-transform_idle="o:1;"
						data-transform_in="y:[100%];sX:1;sY:1;o:0;s:1150;e:Power4.easeInOut;"
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
						Vacancies available
					</div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption large_white tp-resizeme"
					data-x="right"
					data-y="270"
					data-start="750"
					data-transform_idle="o:1;"
					data-transform_in="o:0;s:2000;e:Power4.easeInOut;">
						<div class="separator-3 light"></div>
					</div>

					<!-- LAYER NR. 3 -->
					<div class="tp-caption medium_white"
					data-x="right"
					data-y="290"
					data-start="750"
					data-transform_idle="o:1;"
					data-transform_in="y:[100%];sX:1;sY:1;s:850;e:Power4.easeInOut;"
					data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
					data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">Looking for a career instead of just a job? <br> Want to work with the best professionals? <br> We are always keen to recruit dedicated and professional staff
					</div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption small_white"
					data-x="right"
					data-y="410"
					data-start="1000"
					data-transform_idle="o:1;"
					data-transform_in="y:[100%];sX:1;sY:1;o:0;s:600;e:Power4.easeInOut;"
					data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
					data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
					data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"><a href="{{url('/register')}}" class="btn btn-dark btn-default btn-animated">Register now <i class="fa fa-arrow-right"></i></a>
					</div>
				</li>
			</ul>
			<div class="tp-bannertimer"></div>
			</div>
		</div>
	</div>
</div>

<div id="page-start"></div>
<section class="pv-30 light-gray-bg clearfix">
	<div class="container">
		<h3 class="title logo-font text-center text-default">Vrm Care</h3>
		<div class="separator"></div>
		<p class="text-center">We are professional Healthcare recruitment agency that is poised for providing <br>well trained and dedicated healthcare staffing, excellent service delivery for a long lasting client satisfaction.</p>
		<br>
		<div class="row grid-space-10">
			<div class="col-md-6 col-lg-3">
				<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
					<span class="icon default-bg circle"><i class="fa fa-plus-square"></i></span>
					<h3>Services</h3>
					<div class="separator clearfix"></div>
					<p>He pride ourselves to offer superior staffing services to our valued clients. <br>We recruit for, and are able to provide a variety of health care workers </p>
					<a href="{{url('/who-we-are')}}" class="btn btn-default btn-animated">Read More <i class="fa fa-angle-double-right"></i></a>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
					<span class="icon default-bg circle"><i class="fa fa-hospital-o"></i></span>
					<h3>Guidance</h3>
					<div class="separator clearfix"></div>
					<p>Undecided or having a question? <br>Feel free to call us at 08001937372 or by email and we will happily provide our guidance and answers.</p>
					<a href="{{url('/contact')}}" class="btn btn-default btn-animated">Contact Us <i class="pl-1 fa fa-envelope"></i></a>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="pv-30 ph-20 default-bg feature-box bordered text-center">
					<span class="icon dark-bg circle"><i class="fa fa-phone"></i></span>
					<h3>Vacancies</h3>
					<div class="separator clearfix"></div>
					<p>VRM CARE Ltd is always keen to recruit the best professionals with the right mix of professional experience and personality.</p>
					<a href="{{url('/register')}}" class="btn btn-default btn-animated">Register now<i class="pl-1 fa fa-sign-in"></i></a>
				</div>
			</div>
			<div class="col-md-6 col-lg-3">
				<div class="pv-30 ph-20 white-bg feature-box bordered text-center">
					<span class="icon default-bg circle"><i class="fa fa-clock-o"></i></span>
					<h3>Pay Rates</h3>
					<div class="separator clearfix"></div>
					<p>We believe that dedication and professionalism must be repaid. We value our team and offer them satisfaction by a payrate that matches their skills.</p>
					<a href="{{url('/payrates')}}" class="btn btn-default btn-animated">Details <i class="pl-1 fa fa-money"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- section end -->

<!-- section start -->
<!-- ================ -->
@include('specialties-banner')
<!-- section end -->

<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
<!-- ================ -->
<!-- footer end -->
</div>
<!-- page-wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster -->
<!-- ================================================== -->

@endsection

