<div class="header-container">
	<div class="header-top colored">
		<div class="container">
			<div class="row">
				<div class="col-3 col-sm-6 col-lg-9">
					<div class="header-top-first clearfix">
						<ul class="social-links circle small clearfix hidden-sm-down">
							<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
						<div class="social-links hidden-md-up circle small">
							<div class="btn-group dropdown">
								<button id="header-top-drop-1" type="button" class="btn dropdown-toggle dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i></button>
								<ul class="dropdown-menu dropdown-animation" aria-labelledby="header-top-drop-1">
									<li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
									<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
								</ul>
							</div>
						</div>
						<ul class="list-inline hidden-md-down">
							<li class="list-inline-item"><i class="fa fa-phone pr-1 pl-2"></i>0800 193 7372</li>
							<li class="list-inline-item"><i class="fa fa-mobile pr-1 pl-2"></i>07444734030</li>
							<li class="list-inline-item"><i class="fa fa-envelope-o pr-1 pl-2"></i> admin@vrmcare.co.uk</li>
						</ul>
					</div>
				</div>
				<div class="col-9 col-sm-6 col-lg-3">

					<!-- header-top-second start -->
					<!-- ================ -->
					<div id="header-top-second"  class="clearfix">

						<!-- header top dropdowns start -->
						<!-- ================ -->
						<div class="header-top-dropdown text-right">
							@if (! Auth::check())
							<div class="btn-group">
								<a href="{{url('/register')}}" class="btn btn-default btn-sm"><i class="fa fa-user pr-2"></i> Register</a>
							</div>
							<!-- <div class="btn-group">
								<button id="header-top-drop-2" type="button" class="btn dropdown-toggle btn-default btn-sm dropdown-toggle--no-caret" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-lock pr-2"></i> Login</button>
								<ul class="dropdown-menu dropdown-menu-right dropdown-animation" aria-labelledby="header-top-drop-2">
									<li>
										<form id="login-form" class="login-form margin-clear">
											<div class="form-group has-feedback">
												<label class="control-label">Email</label>
												<input type="text" name="email" class="form-control" placeholder="">
												<i class="fa fa-user form-control-feedback"></i>
											</div>
											<div class="form-group has-feedback">
												<label class="control-label">Password</label>
												<input type="password" name="password" class="form-control" placeholder="">
												<i class="fa fa-lock form-control-feedback"></i>
											</div>
											<button type="submit" class="btn btn-gray btn-sm">Log In</button>
											<span class="pl-1 pr-1">or</span>
											<a class="btn btn-default btn-sm" href="{{url('/register')}}">
												Sign Up
											</a>
											<ul>
												<li><a href="#">Forgot your password?</a></li>
											</ul>
										</form>
									</li>
								</ul>
							</div> -->
							@else
							<div class="btn-group">
								<a href="{{ route('logout') }}" class="btn btn-default btn-sm" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</div>
							@endif
						</div>
						<!--  header top dropdowns end -->
					</div>
					<!-- header-top-second end -->
				</div>
			</div>
		</div>
	</div>
	<header class="header fixed fixed-desktop clearfix">
		<div class="container">
			<div class="row">
				<div class="col-md-auto hidden-md-down pl-3">
					<div class="header-first clearfix">
						<div id="logo" class="logo">
							{{-- <a href="{{url('/')}}"><img id="logo_img" src="images/logo_dark_cyan.png" alt="The Project"></a> --}}
							<a href="{{url('/')}}">
								<img src="{{asset('images/logo_new.png')}}">
							</a>
						</div>
						{{-- <div class="site-slogan">
							Nursing Agency
						</div> --}}
					</div>
				</div>
				<div class="col-lg-8 ml-lg-auto">
					<div class="header-second clearfix">
						<div class="main-navigation main-navigation--mega-menu  animated">
							<nav class="navbar navbar-expand-lg navbar-light p-0">
								<div class="navbar-brand clearfix hidden-lg-up">
									<div id="logo-mobile" class="logo">
										<a href="{{url('/')}}">
											<img src="{{asset('images/logo_new.png')}}">
										</a>
									</div>

								</div>
								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
									<span class="navbar-toggler-icon"></span>
								</button>
								<div class="collapse navbar-collapse" id="navbar-collapse-1">
									<ul class="navbar-nav ml-xl-auto">
										<li id="nav-home" class="nav-item dropdown mega-menu mega-menu--wide">
											<a href="{{url('/')}}" class="nav-link" id="first-dropdown" aria-haspopup="true" aria-expanded="false">Home</a>
										</li>
										<li id="nav-about" class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" id="sixth-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About us</a>
											<ul class="dropdown-menu" aria-labelledby="sixth-dropdown">
												<li id="sub-who-we-are"><a href="{{url('/about-us')}}">Who we are</a></li>
												{{-- <li id="sub-what-we-do"><a href="{{url('/what-we-do')}}">What we do</a></li> --}}
												<li id="sub-vision"><a href="{{url('/our-vision')}}">Our Vision</a></li>
											</ul>
										</li>
										<li id="nav-work" class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" id="sixth-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Working with us</a>
											<ul class="dropdown-menu" aria-labelledby="sixth-dropdown">
												<li id="sub-work"><a href="{{url('/work-with-us')}}">Work with us</a></li>
												<li id="sub-payrates"><a href="{{url('/payrates')}}">Payrates</a></li>
												<li id="sub-policies"><a href="{{url('/policies')}}">Policies</a></li>
											</ul>
										</li>
										<li id="nav-contact" class="nav-item dropdown mega-menu mega-menu--wide">
											<a href="{{url('/contact')}}" class="nav-link" id="first-dropdown" aria-haspopup="true" aria-expanded="false">Contact</a>
										</li>
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
</div>