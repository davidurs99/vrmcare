FROM composer as vendor

WORKDIR /app

COPY . .

RUN composer install  \
    --ignore-platform-reqs \
    --no-ansi \
    --no-dev \
    --no-interaction \
    --no-scripts \
    --optimize-autoloader

FROM node as frontend

COPY . ./
COPY resources ./resources
RUN npm install
RUN npm run production

FROM php:7.2-apache-buster


WORKDIR /var/www/html

COPY --from=vendor /app .
COPY --from=frontend /public ./public


ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN a2enmod rewrite

# Extensions
RUN docker-php-ext-install pdo_mysql

# Clear cache
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    rm /var/log/lastlog /var/log/faillog


RUN chown -R www-data:www-data /var/www/html