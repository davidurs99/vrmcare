<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@welcome')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact', 'PublicController@contact')->name('contact');
Route::get('/work-with-us', 'PublicController@workWithUs')->name('work');
Route::get('/payrates', 'PublicController@payrates')->name('payrates');
Route::get('/policies', 'PublicController@policies')->name('policies');
Route::get('/about-us', 'PublicController@aboutUs')->name('about');
Route::get('/our-vision', 'PublicController@ourVision')->name('vision');
Route::post('/contact', 'PublicController@sendContactMessage')->name('contact-message');
Route::get('/vrmadmin', 'PublicController@admin')->name('admin');
Route::post('/narative', 'PublicController@narative')->name('narative');
Route::get('/download/{filename}', 'PublicController@downloadFile')->name('download');

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
