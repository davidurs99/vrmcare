<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NarativeMessage extends Mailable
{
    use Queueable, SerializesModels;


    public $name;
    public $email;
    public $number;
    public $sector;
    public $companyName;
    public $companySize;
    public $profit;
    public $other;
    public $phone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $number, $sector, $companyName, $companySize, $profit,
        $other, $phone)
    {
        $this->name = $name;
        $this->email = $email;
        $this->number = $number;
        $this->sector = $sector;
        $this->companyName = $companyName;
        $this->companySize = $companySize;
        $this->profit = $profit;
        $this->other = $other;
        $this->phone = $phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.narative');
    }
}
