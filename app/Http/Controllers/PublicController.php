<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessage;
use App\Mail\NarativeMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PublicController extends Controller
{

	public function __construct()
	{
		header("Access-Control-Allow-Origin: *");
	}

    public function admin()
    {
        $scripts = ['js/login.js'];
        return view('admin', ['scripts' => $scripts]);
    }

    public function contact()
    {
		View::share('navHighlightId', 'contact');
        $scripts = ['js/contact.js'];
        return view('contact', ['scripts' => $scripts]);
    }

    public function sendContactMessage(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $message = $request->input('message');
        $subject = $request->input('subject');
        $destination = config('app.email');
        Mail::to($destination)->send(new ContactMessage($message, $email, $name, $subject));
        // Mail::to("davidurs1@gmail.com")->send(new ContactMessage("test", "test", "test", "test"));
        return "true";
    }

    public function welcome()
    {
		View::share('navHighlightId', 'home');
        $scripts = ['js/login.js'];
        return view('welcome', ['scripts' => $scripts]);
    }

    public function workWithUs()
    {
        View::share('navHighlightId', 'work');
        View::share('submenuHighlightId', 'work');
        return view('work');
    }

    public function policies()
    {
        View::share('navHighlightId', 'work');
        View::share('submenuHighlightId', 'policies');
        // dd(Storage::allFiles('policies'));
        View::share('policies', Storage::allFiles('policies'));
        return view('policies');
    }

    public function downloadFile($fileName) 
    {
        return response()->download(
            storage_path('app/policies/' . $fileName)
        );
    }

    public function payrates()
    {
        View::share('navHighlightId', 'work');
        View::share('submenuHighlightId', 'payrates');
        return view('payrates');
    }

    public function aboutUs()
    {
        View::share('navHighlightId', 'about');
        View::share('submenuHighlightId', 'who-we-are');
        return view('about');
    }

    public function ourVision()
    {
        View::share('navHighlightId', 'about');
        View::share('submenuHighlightId', 'vision');
        return view('vision');
    }

    public function narative(Request $request)
    {
        // return "true";

        $name = $request->input('name');
        $email = $request->input('email');
        $number = $request->input('number');
        $sector = $request->input('sector');
        $companyName = $request->input('companyName');
        $companySize = $request->input('companySize');
        $profit = $request->input('profit');
        $other = $request->input('other');
        $phone = $request->input('phone');
        Mail::to("davidurs1@gmail.com")->send(new NarativeMessage($name, $email, $number, $sector, $companyName, $companySize, $profit, $other, $phone ));
        return "true";
    }


}
