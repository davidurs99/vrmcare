$(function() {
    $("#contact-form").validate({
        rules: {
            "email": {
                required: true,
                email: true,
                maxlength: 30
            },
            "name": {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            "message": {
                required: true,
                minlength: 6,
                maxlength: 400
            }
        },
        submitHandler: function() {
            $('#success-alert').addClass('hidden');
            sendContact();
        }
    })

    function sendContact() {
        $.post('/contact', $("#contact-form").serialize(), function(data) {
            $("#contact-form")[0].reset();
            $("#success-alert").removeClass('hidden');
            setTimeout(function() {
                $('#success-alert').addClass('hidden');
            }, 7000);
        }).fail(function(xhr, status, error) {
            alert('failed');
        });
    }

});