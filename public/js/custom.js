/*
 * Version: 2.1
 */

// Notify Plugin - Code for the demo site of HtmlCoder
// You can delete the code below
//-----------------------------------------------
(function($) {

	"use strict";

	$(document).ready(function() {
		$("#nav-" + navHighlightId).addClass("active");
		$("#sub-" + submenuHighlightId).addClass("active");
	}); // End document ready

})(jQuery);
