$(function() {

    $("#login-form").validate({
        rules: {
            "email": {
                required: true,
                email: true
            },
            "password": {
                required: true
            }
        },
        submitHandler: function() {
            sendlogin();
        }
    })

    function sendlogin() {
        $.post('/login', $("#login-form").serialize(), function(data) {
            window.location = "/";
        }).fail(function(xhr, status, error) {
            $("#login-form input[type=password]").val("");
        });
    }

});