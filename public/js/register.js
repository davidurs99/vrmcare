$(function() {

    $("#register-form").validate({
        rules: {
            "name": {
                required: true
            },
            "email": {
                required: true,
                email: true
            },
            // "password": {
            //     required: true
            // },
            // "password_confirmation": {
            //     required: true,
            //     equalTo: '#password'
            // }
        }
        ,
        submitHandler: function() {
            console.log('in submit');
            sendRegister();
        }
    })

    function sendRegister() {
        $.post('/register', $("#register-form").serialize(), function(data) {
            $("#register-form")[0].reset();
            $("#success-alert").removeClass('hidden');
            setTimeout(function() {
                window.location = "/";
                $('#success-alert').addClass('hidden');
            }, 4000);

        })
    }

});